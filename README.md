# gpt

A Python library to parse and plot (and maybe other things?) the GDF output from General Particle Tracer (GPT).

I don't plan to update or maintain this repo except as I use it for my projects.

## License

MIT

## Project status

Unmaintained
